﻿using UnityEngine;
using System.Collections;

public class CylinderHolder : MonoBehaviour {

    public Cylinder currentCylinder;

	public void UpdateDisplay(float wear)
    {
        if(currentCylinder != null)
        {
            currentCylinder.UpdateDisplay(wear);
            currentCylinder.transform.localPosition = Vector3.zero;
            currentCylinder.fixedD = false;
        }
        else
        {
            currentCylinder = GetComponentInChildren<Cylinder>();
            if(currentCylinder != null)
            {
                currentCylinder.fixedD = false;
                currentCylinder.UpdateDisplay(wear);
                currentCylinder.transform.localPosition = Vector3.zero;

            }
        }
    }
}
