﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DraggableObjects : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{

    public bool beingDragged = false;
    public Vector3 originalPos;

    void Start()
    {
        originalPos = transform.position;
    }

    void Update()
    {

  
        if (beingDragged == true)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = transform.position.z;
            transform.position = mousePos;
        }
        if(beingDragged == false)
        {
            if (transform.position.x < 11 || transform.position.x > 285 || transform.position.y > 117 || transform.position.y < 9)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(150, 100, 0), Time.deltaTime * 100);
            }
        }

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        beingDragged = true;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        beingDragged = false;
    }

    public void OnDrag(PointerEventData eventData)
    {

        beingDragged = true;

    }

    public void OnDrop(PointerEventData eventData)
    {
    }
}