﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;


public class Cylinder : CarPiece
{

    public LayerMask holderMask;
    public bool fixedD = false;
    protected BoxCollider2D boxCollider;
    protected CylinderHolder currentHolder;
    protected SpriteRenderer spriteRenderer;

    void Awake()
    {
        fixedD = false;
        draggableObject = GetComponent<DraggableObjects>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public void UpdateDisplay(float wear)
    {
        string amoun = "Bad";
        string rust = "";
        if(wear < 0)
        {
            rust = "_Rust";
        }

        float weard = Mathf.Abs(wear);

        if(weard < 0.25f)
        {
            amoun = "VeryBad";
        }else if(weard < 0.5f)
        {
            amoun = "Bad";

        }else if(weard < 0.75f)
        {
            amoun = "Good";
        }
        else
        {
            amoun = "Great";
        }

        spriteRenderer.sprite = GameManager.instance.parts["Cylinder_" + amoun + rust];

    }

    void Update()
    {
        if (draggableObject.beingDragged == false)
        {
            RaycastHit2D holderCheck = Physics2D.BoxCast(transform.position + new Vector3(boxCollider.offset.x, boxCollider.offset.y), boxCollider.size, 0, Vector3.zero, 0, holderMask);

            if (holderCheck.collider != null)
            {
                if (holderCheck.collider.CompareTag("CylinderHolder"))
                {
                    CylinderHolder holder = holderCheck.collider.GetComponent<CylinderHolder>();
                    holder.currentCylinder = this;
                    currentHolder = holder;
                    transform.SetParent(holder.transform);
                    transform.localPosition = Vector3.zero;
                }
            }
            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector3(22, 30), 0, Vector3.zero, 0, 1 << LayerMask.NameToLayer("TrashCan"));
            if (hit.collider != null && fixedD == false)
            {
                fixedD = true;

                GameManager.instance.activeMotor.cylinderFixed += 1;
                UpdateDisplay(1);
            }
        }
    }

   
}
