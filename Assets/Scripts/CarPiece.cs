﻿using UnityEngine;
using System.Collections;

public class CarPiece : MonoBehaviour {

    public DraggableObjects draggableObject;

    void Awake()
    {
        draggableObject = GetComponent<DraggableObjects>();

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CheckTrashCan()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector3(22, 30), 0, Vector3.zero, 0, 1 << LayerMask.NameToLayer("TrashCan"));
        if(hit.collider != null)
        {
            Debug.Log("Hit TrashCan");
        }
    }
}
