﻿using UnityEngine;
using System.Collections;

public class OilCapHolder : MonoBehaviour {

    public OilCap currentOilCap;
    public GameObject oilCollector;

    void Awake()
    {
        OnPlaceCap();
    }

	public void OnRemoveCap()
    {
        oilCollector.gameObject.SetActive(true);
    }

    public void OnPlaceCap()
    {
        oilCollector.gameObject.SetActive(false);
    }


}
