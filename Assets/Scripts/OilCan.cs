﻿using UnityEngine;
using System.Collections;

public class OilCan : MonoBehaviour {

    public ParticleSystem oilDropping;
    public DraggableObjects draggableObject;

    float angle = -94.7f;
	// Use this for initialization
	void Start () {
        if (oilDropping.isPlaying)
        {
            oilDropping.Stop();
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(draggableObject.beingDragged && Input.GetMouseButton(1))
        {
            if(!oilDropping.isPlaying)
            {
                oilDropping.Play();
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, angle),Time.deltaTime*300);
            RaycastHit2D hit = Physics2D.Raycast(oilDropping.transform.position, Vector3.down, 100, 1 << LayerMask.NameToLayer("OilPan"));
            if(hit.collider != null)
            {
                if(hit.collider.CompareTag("OilInsert"))
                {
                    GameManager.instance.GainOil();
                }
            }
        }
        else
        {
            if(oilDropping.isPlaying)
            {
                oilDropping.Stop();
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime*300);

        }
	}
}
