﻿using UnityEngine;
using System.Collections;

public class Filter : CarPiece{

    public FilterHolder filterHolder;

    public LayerMask holderMask;
    public bool off;
    protected SpriteRenderer spriteRenderer;
    protected BoxCollider2D boxCollider;

    void Awake()
    {
        draggableObject = GetComponent<DraggableObjects>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public void UpdateDisplay(float filterWear)
    {
        string amoun = "";
        if(filterWear < 0.5f)
        {
            amoun = "Bad";
        }
        else
        {
            amoun = "Good";
        }
        spriteRenderer.sprite = GameManager.instance.parts["Filter_" + amoun];
        

    }

    public void Reset()
    {
        transform.localPosition = Vector3.zero;
    }

    void Update()
    {
        if (draggableObject.beingDragged == false)
        {

            RaycastHit2D holderCheck = Physics2D.BoxCast(transform.position + new Vector3(boxCollider.offset.x, boxCollider.offset.y), boxCollider.size, 0, Vector3.zero, 0, holderMask);

            if (holderCheck.collider != null)
            {
                if (holderCheck.collider.CompareTag("FilterHolder"))
                {

                    FilterHolder och = holderCheck.collider.GetComponent<FilterHolder>();
                    och.filter = this;
                    transform.SetParent(och.transform);
                    off = false;

                    transform.localPosition = Vector3.zero;
                }
            }

            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector3(22, 30), 0, Vector3.zero, 0, 1 << LayerMask.NameToLayer("TrashCan"));
            if (hit.collider != null)
            {
                UpdateDisplay(1);
                GameManager.instance.activeMotor.filterWear = 1;
            }
        }
        else
        {

            off = true;
        }
    }

}
