﻿using UnityEngine;
using System.Collections;

public enum CarState
{
    LookingForStation,
    ParkedInStation,
    BeingWorkedOn,
    LeavingStation,
    Leaving
}

public class Car : MonoBehaviour {

    public ParticleSystem gasSystem;
    public bool lookingForStation;
    protected Transform parkingSpot;
    protected Transform backUpSpot;
    protected CarState carState;
    protected SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
        CarAssets carAssets = Resources.Load<CarAssets>("Cars/CarsAsset");
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = carAssets.cars[Random.Range(0, carAssets.cars.Count)].image;
        carState = CarState.LookingForStation;
        lookingForStation = true;
        parkingSpot = GameObject.FindGameObjectWithTag("ParkingSpot").transform;
        backUpSpot = GameObject.FindGameObjectWithTag("BackUpSpot").transform;
        
	}
	
	// Update is called once per frame
	void Update () {
	      if(carState == CarState.LookingForStation)
          {
              LookForStation();
          }else if(carState == CarState.ParkedInStation)
          {
              if(Input.GetKeyDown(KeyCode.Alpha4))
              {
                  if(GameManager.instance.CarIsReady())
                  {
                      carState = CarState.LeavingStation;
                      GameManager.instance.DisableMotorAndUI();
                  }
              }

          }else if(carState == CarState.LeavingStation)
          {
              LeavingStation();
          }
          else if(carState == CarState.Leaving)
          {
              LeaveWorld();
          }
	}

    public void LookForStation()
    {
        if (Vector3.Distance(transform.position, parkingSpot.position) > 1)
        {
            transform.position = Vector3.MoveTowards(transform.position, parkingSpot.position, Time.deltaTime * 40);
        }
        else
        {
            TurnOffIgnition();
            ToParkedInStation();
        }
    }

    public void ToParkedInStation()
    {
        carState = CarState.ParkedInStation;
        GameManager.instance.EnableMotorAndUI();
    }

    public void LeavingStation()
    {
        if (Vector3.Distance(transform.position, backUpSpot.position) > 1)
        {
            transform.position = Vector3.MoveTowards(transform.position, backUpSpot.position, Time.deltaTime * 40);
        }
        else
        {
            TurnOnIgnition();
            spriteRenderer.sortingLayerName = "UI";
            carState = CarState.Leaving;
        }
    }

    public void LeaveWorld()
    {
        Vector3 leftDir = Vector3.left;
        leftDir.x *= 40;
        transform.position += leftDir * Time.deltaTime;
        if(transform.position.x < -100)
        {
            GameManager.instance.SpawnCar();
            GameObject.Destroy(gameObject);
        }
    }

    public void TurnOffIgnition()
    {
        if(gasSystem.isPlaying)
        {
            gasSystem.Stop();
        }
    }

    public void TurnOnIgnition()
    {
        if (!gasSystem.isPlaying)
        {
            gasSystem.Play();
        }
    }
}
