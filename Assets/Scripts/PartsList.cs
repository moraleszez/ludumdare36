﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName="Parts",menuName="Parts",order=1)]
public class PartsList : ScriptableObject {

    public List<PartsName> parts;

}

[System.Serializable]
public class PartsName
{
    public string partName;
    public Sprite image;
}
