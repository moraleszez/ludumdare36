﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName="Car Assets",menuName="CarAssets",order=1)]
public class CarAssets : ScriptableObject{
    public List<CarContainer> cars;
	
}

[System.Serializable]
public class CarContainer
{
    public string name;
    public Sprite image;
}
