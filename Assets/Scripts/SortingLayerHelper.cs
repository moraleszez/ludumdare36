﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SortingLayerHelper : MonoBehaviour {


	// Use this for initialization
	void Start () {
        GetComponent<Renderer>().sortingLayerName = "UI";
        GetComponent<Renderer>().sortingOrder = 200;

    }

    // Update is called once per frame
    void Update () {
	
	}
}
