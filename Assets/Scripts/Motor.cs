﻿using UnityEngine;
using System.Collections;

public class Motor : MonoBehaviour {

    public CylinderHolder cylinder_1;
    public CylinderHolder cylinder_2;
    public CylinderHolder cylinder_3;

    public OilMeasureRod oilMeasureRod;
    public FilterHolder filterHolder;

    public GameObject frontMotor;
    public GameObject backMotor;

    public float oil;//Set from out side determines rod percent
    public float oilAge;//determin from outside auto adjust rods to match
    public float cylinder_1wear;
    public float cylinder_2wear;
    public float cylinder_3wear;

    public float cylinderFixed = 0;

    public float filterWear;
	// Use this for initialization
	void Start () {
        Randomizer();
        cylinder_1.UpdateDisplay(cylinder_1wear);
        cylinder_2.UpdateDisplay(cylinder_2wear);
        cylinder_3.UpdateDisplay(cylinder_3wear);
        oilMeasureRod.UpdateDisplay(oil, oilAge);
        filterHolder.UpdateDisplay(filterWear);
        SwitchSides(2);
	}

    public void UpdateSources()
    {
        cylinderFixed = 0;
        cylinder_1.UpdateDisplay(cylinder_1wear);
        cylinder_2.UpdateDisplay(cylinder_2wear);
        cylinder_3.UpdateDisplay(cylinder_3wear);
        oilMeasureRod.UpdateDisplay(oil, oilAge);
        filterHolder.UpdateDisplay(filterWear);
        SwitchSides(2);
    }

    public void Randomizer()
    {
        oil = Random.Range(0f, 1f);
        oilAge = Random.Range(0f, 1f);
        cylinder_1wear = Random.Range(-1f, 1f);
        cylinder_2wear = Random.Range(-1f, 1f);
        cylinder_3wear = Random.Range(-1f, 1f);
        filterWear = Random.Range(0, 0.6f);
    }
	
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            SwitchSides();
        }
    }

    public void SwitchSides()
    {
        if(frontMotor.gameObject.activeInHierarchy == true)
        {
            backMotor.gameObject.SetActive(true);
            frontMotor.gameObject.SetActive(false);
        }
        else if (backMotor.gameObject.activeInHierarchy == true)
        {
            backMotor.gameObject.SetActive(false);
            frontMotor.gameObject.SetActive(true);
        }

    }

    public void SwitchSides(int m)
    {
        if (m == 0)
        {
            backMotor.gameObject.SetActive(true);
            frontMotor.gameObject.SetActive(false);
        }
        else if (m == 2)
        {
            backMotor.gameObject.SetActive(false);
            frontMotor.gameObject.SetActive(true);
        }

    }

    public void DropOil()
    {
        oil -= Time.deltaTime*0.3f;
        oilMeasureRod.UpdateDisplay(oil, oilAge);
        oil = Mathf.Clamp(oil, 0, 1f);
    }

    public void FillOil()
    {
        oil += Time.deltaTime*0.3f;
        oilMeasureRod.UpdateDisplay(oil, oilAge);
        oil = Mathf.Clamp(oil, 0, 1f);
    }
}
