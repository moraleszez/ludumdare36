﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum GameState
{
    Opening,
    Descending,
    CarLoaded,
    CarLeaving
}

public class GameManager : MonoBehaviour {

    private static GameManager gameManager;

    public static GameManager instance
    {
        get
        {
            if (!gameManager)
            {
                gameManager = FindObjectOfType(typeof(GameManager)) as GameManager;

                if (!gameManager)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {

                }
            }

            return gameManager;
        }
    }


    public Motor activeMotor;
    public Sprite CleanMeasureRod;
    public Sprite FullMeasureRod;
    public GameObject UiSpot;
    public GameObject textSpot;

    public Dictionary<string, Sprite> parts;
    public List<Text> messages;
    public int currentMessage;
    protected Car prefabCar;
    protected GameObject carSpawnPoint;
    public AudioSource mainTheme;

    void Awake()
    {
        LoadParts();
        DisableMotorAndUI();
#if UNITY_STANDALONE
        Screen.SetResolution(600, 400, false);
#endif


    }

    void Start()
    {
        prefabCar = Resources.Load<Car>("CarPrefab");
        carSpawnPoint = GameObject.FindGameObjectWithTag("CarSpawnSpot");
        textSpot.gameObject.SetActive(false);
    }

    public void StartService()
    {
        Car m = GameObject.Instantiate(prefabCar);
        m.transform.position = carSpawnPoint.transform.position;
        if(!mainTheme.isPlaying)
        {
            mainTheme.Play();
        }
    }

    void LoadParts()
    {
         Sprite[] sprites = Resources.LoadAll<Sprite>("OtherPieces");
         parts = new Dictionary<string, Sprite>();
           foreach (Sprite sprite in sprites)
           {
               parts.Add(sprite.name, sprite);
           }
           Debug.Log(parts.Count);
    }

    public void EnableMotorAndUI()
    {
        UiSpot.gameObject.SetActive(true);
        activeMotor.gameObject.SetActive(true);
    }

    public void DisableMotorAndUI()
    {
        UiSpot.gameObject.SetActive(false);
        activeMotor.gameObject.SetActive(false);
    }

    public void EnableUI()
    {
        UiSpot.gameObject.SetActive(true);
    }

    public void DisableUI()
    {
        UiSpot.gameObject.SetActive(false);
    }

    public void SwitchMotorSides()
    {
        if(activeMotor != null)
        {
            activeMotor.SwitchSides();
        }
    }

    public void GainOil()
    {
        if(activeMotor != null)
        {
            activeMotor.FillOil();
        }
    }

    public void LoseOil()
    {
        if(activeMotor != null)
        {
            activeMotor.DropOil();
        }
    }

    public bool CarIsReady()
    {
        if(activeMotor != null)
        {
            bool ready = true;
            if(activeMotor.oil < 0.9f)
            {
                ready = false;

            }

            if(activeMotor.oilAge < 0.92f)
            {
                ready = false;
            }

            if(activeMotor.filterWear < 0.7f)
            {
                ready = false;
            }
            if(activeMotor.cylinderFixed != 3)
            {
                ready = false;
            }

            return ready;
        }
        return false;
    }

    public void SpawnCar()
    {
        StartCoroutine(SpawnOther());
    }

    IEnumerator SpawnOther()
    {
        textSpot.gameObject.SetActive(true);
        messages[currentMessage].gameObject.SetActive(true);
        yield return new WaitForSeconds(4.7f);
        textSpot.gameObject.SetActive(false);
        messages[currentMessage].gameObject.SetActive(false);
        currentMessage += 1;
        if (currentMessage > messages.Count)
        {
#if UNITY_STANDALONE
            Application.Quit();
#endif

#if UNITY_WEBGL
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
#endif
        }
        messages[currentMessage].gameObject.SetActive(false);

        yield return new WaitForSeconds(Random.Range(0.8f, 2.3f));
        Car m = GameObject.Instantiate(prefabCar);
        m.transform.position = carSpawnPoint.transform.position;
        GameManager.instance.activeMotor.Randomizer();
        GameManager.instance.activeMotor.UpdateSources();
    }

}
