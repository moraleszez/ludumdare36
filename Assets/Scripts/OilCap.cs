﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class OilCap : CarPiece
{
    public OilCapHolder currentOilCapHolder;
    public LayerMask holderMask;
    protected BoxCollider2D boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {

        if(draggableObject.beingDragged == true)
        {
            if (currentOilCapHolder != null)
                {
                    currentOilCapHolder.OnRemoveCap();
                }
        }

        if (draggableObject.beingDragged == false)
        {
            RaycastHit2D holderCheck = Physics2D.BoxCast(transform.position + new Vector3(boxCollider.offset.x,boxCollider.offset.y),boxCollider.size,0,Vector3.zero,0,holderMask);

            if(holderCheck.collider != null)
            {
                if(holderCheck.collider.CompareTag("OilHolder"))
                {
                    OilCapHolder och = holderCheck.collider.GetComponent<OilCapHolder>();
                    och.currentOilCap = this;
                    if(currentOilCapHolder == null)
                    {
                        currentOilCapHolder = och;
                    }
                    transform.SetParent(och.transform);

                    transform.localPosition = Vector3.zero;
                    och.OnPlaceCap();
                }

            }
          
                
        }

       
    }
}
