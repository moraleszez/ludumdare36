﻿using UnityEngine;
using System.Collections;

public class FilterHolder : MonoBehaviour {

    public Filter filter;
    public ParticleSystem oilDropping;
	
    void Update()
    {
        CheckSpillOil();
    }

    public void UpdateDisplay(float filterwear)
    {
        if(filter != null)
        {
            filter.UpdateDisplay(filterwear);
            filter.Reset();
        }
    }

    public void CheckSpillOil()
    {
        if(oilDropping.isPlaying)
        {
            GameManager.instance.LoseOil();
        }

        if (filter.off == true)
        {
            if (!oilDropping.isPlaying)
            {
                oilDropping.Play();
            }
        }
        else
        {
            if (oilDropping.isPlaying)
            {
                oilDropping.Stop();
            }
        }

        if(GameManager.instance.activeMotor != null && GameManager.instance.activeMotor.oil < 0.05f)
        {
            if (oilDropping.isPlaying)
            {
                oilDropping.Stop();
            }

        }
    }

}
