﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class OilMeasureRod : CarPiece{

    protected SpriteRenderer spriteRenderer;

    void Awake()
    {
        draggableObject = GetComponent<DraggableObjects>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void UpdateDisplay(float oil, float oilAge)
    {
        string fillAmou = "Low";
        if(oil > 0.8f)
        {
            fillAmou = "Full";
        }

        string rusted = "Bad";
        if(oilAge > 0.3f && oilAge < 0.6f)
        {
            rusted = "Med";
        }
        else if(oilAge >= 0.6f)
        {
            rusted = "Good";
        }
        spriteRenderer.sprite = GameManager.instance.parts["Oil_" + rusted + "_" + fillAmou];
        if (oil <= 0.05)
        {
            spriteRenderer.sprite = GameManager.instance.CleanMeasureRod;
            if (GameManager.instance.activeMotor != null)
            {
                GameManager.instance.activeMotor.oilAge = 1;
            }
        }
    }

    void Update()
    {
        if(draggableObject.beingDragged == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, draggableObject.originalPos, Time.deltaTime * 50);
        }
    }

}
